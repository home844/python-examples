#!/usr/bin/python3

#Lhasa: Easy Math from sadservers.com
#scores.txt
#1 4.5
#2 12.7
#3 5.4
#4 8.9

#Before this script:
#awk -F ' ' '{print $2}' scores.txt > $filename

filename = 'second_column'

with open(filename) as file:
    arr_float = [] 
    buff_item = ''
    num_lines = 0

    # read every character in line
    for char in file.read():
        if char != '\n':
            buff_item = buff_item + char
        else:
            if buff_item != '':
                arr_float.append(float(buff_item))
                buff_item = ''
                num_lines = num_lines + 1

    print("Array:", arr_float)
    print("Number of lines:", num_lines)
    print("Average:", sum(arr_float) / num_lines)
